#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "so_stdio.h"

#define BUFSIZE 4096
#define FLAG_EOF 1
#define FLAG_ERR 2
#define FLAG_LAST_R 4
#define FLAG_LAST_W 8

#define MODE_R 1
#define MODE_RPLUS 2
#define MODE_W 3
#define MODE_WPLUS 4
#define MODE_A 5
#define MODE_APLUS 6

struct _so_file {
	int fd; //File descriptor-ul
	unsigned char *buf; //Buffer-ul
	int buf_size; //Numarul de biti cititi in buffer
	int buf_pos; //Pozitia in buffer
	int flags; //Flagurile setate
	int mode; //Modul in care am deschis fisierul
};

SO_FILE *so_fopen(const char *pathname, const char *mode)
{
	int fd = -1;
	SO_FILE *stream;

	stream = malloc(sizeof(SO_FILE));

	//Deshiderea in functie de mode
	if (strcmp(mode, "r") == 0) {
		fd = open(pathname, O_RDONLY);
		stream->mode = MODE_R;
	}

	if (strcmp(mode, "r+") == 0) {
		fd = open(pathname, O_RDWR);
		stream->mode = MODE_RPLUS;
	}

	if (strcmp(mode, "w") == 0) {
		fd = open(pathname, O_WRONLY | O_CREAT | O_TRUNC, 0644);
		stream->mode = MODE_W;
	}

	if (strcmp(mode, "w+") == 0) {
		fd = open(pathname, O_RDWR | O_CREAT | O_TRUNC, 0644);
		stream->mode = MODE_WPLUS;
	}

	if (strcmp(mode, "a") == 0) {
		fd = open(pathname, O_WRONLY | O_CREAT | O_APPEND, 0644);
		stream->mode = MODE_A;
	}

	if (strcmp(mode, "a+") == 0) {
		fd = open(pathname, O_RDWR | O_CREAT | O_APPEND, 0644);
		stream->mode = MODE_APLUS;
	}

	if (fd < 0) {
		free(stream);
		return NULL;
	}

	//Crearea structurii
	stream->fd = fd;
	stream->flags = 0;
	stream->buf_pos = 0;
	stream->buf_size = 0;
	stream->buf = malloc(BUFSIZE * sizeof(unsigned char));

	return stream;
}

int so_fclose(SO_FILE *stream)
{
	int rc;

	//Daca ultima operatie a fost the write, golim bufferul
	if (stream->flags & FLAG_LAST_W)
		so_fflush(stream);


	rc = close(stream->fd);

	//Verificam daca s-a inchis
	if (rc < 0) {
		free(stream->buf);
		free(stream);

		return SO_EOF;
	}

	//Verificam daca am abut vreo eroare
	if (so_ferror(stream)) {
		free(stream->buf);
		free(stream);

		return SO_EOF;
	}

	free(stream->buf);
	free(stream);

	return 0;
}

int so_fileno(SO_FILE *stream)
{
	return stream->fd;
}

int so_fflush(SO_FILE *stream)
{
	int rc;

	//Incercam sa golim doar daca bufferul contine ceva
	if (stream->buf_pos == 0)
		return 0;


	rc = write(stream->fd, stream->buf, stream->buf_pos);

	stream->buf_pos = 0;

	if (rc < 0) {
		stream->flags |= FLAG_ERR;
		return SO_EOF;
	}

	return 0;
}

int so_fseek(SO_FILE *stream, long offset, int whence)
{
	int rc;

	//Daca bufferul contine bytes de scris, dam flush
	if (stream->flags & FLAG_LAST_W)
		so_fflush(stream);

	//Daca contine bytes cititi, invalidam datele
	if (stream->flags & FLAG_LAST_R) {
		stream->buf_pos = 0;
		stream->buf_size = 0;
	}

	rc = lseek(stream->fd, offset, whence);

	if (rc < 0) {
		stream->flags |= FLAG_ERR;
		return SO_EOF;
	}

	return 0;
}

long so_ftell(SO_FILE *stream)
{
	//Acem cazuri diferite daca ultima operatie a fost de citire sau de scriere
	//In ambele cazuri avem un offset in plus
	if (stream->flags & FLAG_LAST_W)
		return lseek(stream->fd, 0, SEEK_CUR) + stream->buf_pos;

	if (stream->flags & FLAG_LAST_R)
		return lseek(stream->fd, 0, SEEK_CUR) + stream->buf_pos - stream->buf_size;

	return lseek(stream->fd, 0, SEEK_CUR);
}

size_t so_fread(void *ptr, size_t size, size_t nmemb, SO_FILE *stream)
{
	size_t read_count = 0;
	unsigned char *read_ptr = ptr;
	int i;
	int j;
	int ret;

	//Citirea in ptr se face byte cu byte
	for (i = 0; i < nmemb; i++) {
		for (j = 0; j < size; j++) {
			//Nu citim direct in ptr pentru a putea face verificarea corect
			ret = so_fgetc(stream);
			if (ret == SO_EOF)
				return read_count;

			read_ptr[i * size + j] = (unsigned char) ret;
		}

		read_count++;
	}

	return read_count;
}

size_t so_fwrite(const void *ptr, size_t size, size_t nmemb, SO_FILE *stream)
{
	size_t write_count = 0;
	const unsigned char *write_ptr = ptr;
	int i;
	int j;

	//Scrierea din ptr se face byte cu byte
	for (i = 0; i < nmemb; i++) {
		for (j = 0; j < size; j++) {
			if (write_ptr[i * size + j] != so_fputc(write_ptr[i * size + j], stream))
				return 0;
		}

		write_count++;
	}

	return write_count;
}

int so_fgetc(SO_FILE *stream)
{
	//Daca ultima operatie a fost un write, facem flush la buffer
	if (stream->flags & FLAG_LAST_W)
		so_fflush(stream);

	//Daca bufferul este gol, il umplem
	if (stream->buf_pos >= stream->buf_size) {
		stream->buf_size = read(stream->fd, stream->buf, BUFSIZE);

		stream->buf_pos = 0;

		//Daca nu s-a citit BUFSIZE, setam flag-ul de EOF
		if (stream->buf_size < BUFSIZE)
			stream->flags |= FLAG_EOF;

		if (stream->buf_size == 0)
			return SO_EOF;

		if (stream->buf_size < 0) {
			stream->flags |= FLAG_ERR;
			return SO_EOF;
		}

	}

	stream->flags &= ~FLAG_LAST_W;
	stream->flags |= FLAG_LAST_R;

	return (int) stream->buf[stream->buf_pos++];
}

int so_fputc(int c, SO_FILE *stream)
{
	//Daca ultima operatie a fost de read, invalidam bufferul
	if (stream->flags & FLAG_LAST_R) {
		stream->buf_pos = 0;
		stream->buf_size = 0;
	}

	stream->buf[stream->buf_pos++] = c;
	//Cand umplem buffer-ul, dam flush
	if (stream->buf_pos == BUFSIZE)
		if (so_fflush(stream) != 0) {
			stream->flags |= FLAG_ERR;
			return SO_EOF;
		}

	stream->flags &= ~FLAG_LAST_R;
	stream->flags |= FLAG_LAST_W;

	return c;
}

int so_feof(SO_FILE *stream)
{
	return stream->flags & FLAG_EOF && stream->buf_size == 0;
}

int so_ferror(SO_FILE *stream)
{
	int saved_flags = stream->flags;
	//Resetam flag-ul de eroare
	stream->flags &= ~FLAG_ERR;
	return saved_flags & FLAG_ERR;
}

SO_FILE *so_popen(const char *command, const char *type)
{
	return NULL;
}

int so_pclose(SO_FILE *stream)
{
	return 0;
}
